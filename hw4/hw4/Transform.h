// Transform header file to define the interface. 
// The class is all static for simplicity
// You need to implement left, up and lookAt
// Rotate is a helper function

// Include the helper glm library, including matrix transform extensions
#pragma once

#if ((defined(__MACH__))&&(defined(__APPLE__)))
	#include <OpenGL/gl.h>
	#include <GLUT/glut.h>
	#include <OpenGL/glext.h>
#elif _WIN32
    #include <GL/glew.h>
    #include <GL/glut.h>
#else
	#include <GL/osmesa.h>
	#include <GL/glu.h>
#endif

#include <glm/glm.hpp>

// glm provides vector, matrix classes like glsl
// Typedefs to make code more readable 

typedef glm::mat3 mat3;
typedef glm::mat4 mat4; 
typedef glm::vec3 vec3; 
typedef glm::vec4 vec4; 

class Transform  
{
public:
	Transform();
	virtual ~Transform();
	static void left(float degrees, vec3& eye, vec3& up);
	static void up(float degrees, vec3& eye, vec3& up);
	static mat4 lookAt(const vec3& eye, const vec3 &center, const vec3& up);
	static mat4 perspective(float fovy, float aspect, float zNear, float zFar);
	static mat3 rotate(const float degrees, const vec3& axis) ;
	static mat4 rotate(vec4 quaternion) ;
	static mat4 rotate(vec3 axis, float angle);
	static mat4 scale(const float &sx, const float &sy, const float &sz) ; 
	static mat4 translate(const float &tx, const float &ty, const float &tz);
};

