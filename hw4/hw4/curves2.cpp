//Name: Loc Do
//Login: cs184-eb
#include "stdafx.h"
#include "WorkingScene.h"

// This file includes the basic functions that your program must fill in.  
// Your assignment consists of filling in parts that say /* YOUR CODE HERE */

// What happens when you drag the mouse to x and y?  
// In essence, you are dragging control points on the curve.
void WorkingScene::drag(int x, int y) {
	/* YOUR CODE HERE */
	//you must figure out how to transform x and y so they make sense
	float newdX = (float)(x - oldx) / width;
	float newdY = (float)(oldy - y)/height;

	//Call updateActivePoint to update the active point to 
	//the new location
	theOnlyCurve->moveActivePoint(newdX, newdY);

	//update oldx, and oldy
	oldx = x;
	oldy = y;

	//make sure scene gets redrawn
	glutPostRedisplay();
}

// Mouse motion.  You need to respond to left clicks (to add points on curve) 
// and right clicks (to delete points on curve) 
void WorkingScene::mouse(int button, int state, int x, int y) {
	//Normalize x and y
	float newX = (float)x / width;
	float newY = 1 - (float)y/height;

	if (theOnlyCurve && state == GLUT_DOWN) {
		if (button == GLUT_LEFT_BUTTON) {
			/* YOUR CODE HERE */
			theOnlyCurve->updateActivePoint(newX, newY);

			//Add the point
			theOnlyCurve->addPoint(newX, newY);
		}
		if (button == GLUT_RIGHT_BUTTON) {	
			/* YOUR CODE HERE */
			theOnlyCurve->updateActivePoint(newX, newY);

			//delete the point
			theOnlyCurve->deleteActivePoint();
		}
	}

	/* YOUR CODE HERE */
	//update oldx, and oldy
	oldx = x;
	oldy = y;

	//make sure scene gets redrawn
	glutPostRedisplay();
}



#include "Bezier.h"

// Bezier drawing function.  This is by deCasteljau or equivalent algorithm. 
// It should support Bezier curves of arbitrary degree/order.
void Bezier::draw(int levelOfDetail) {

	connectTheDots();

	Pvector::iterator p;

	p = points.begin();

	/* YOUR CODE HERE */
	int degree = points.size() - 1;

	if (degree > 1){
		vector<Point> c;

		for (int j = 0; j <= levelOfDetail; j++){
			vector<vector<Point> > tempP(degree + 1);

			float u = (float)j / levelOfDetail;

			for (int level = degree; level >= 0; level--){
				if (level == degree){
					for (int i = 0; i <= level; i++){
						tempP[level].push_back(points[i]);
					}
				}
				else
					for (int i = 0; i <= level; i++){
						float newX, newY;

						newX = (1 - u)*tempP[level + 1][i].x + u*tempP[level + 1][i + 1].x;
						newY = (1 - u)*tempP[level + 1][i].y + u*tempP[level + 1][i + 1].y;
						tempP[level].push_back(Point(newX, newY));
					}
			}

			c.push_back(Point(tempP[0][0].x, tempP[0][0].y));
		}

		for (int i = 0; i < c.size() - 1; i++){
			drawLine(c[i].x, c[i].y, c[i + 1].x, c[i + 1].y);
		}
	}
}



#include "Bspline.h"

// The B-Spline drawing routine.  
// Remember to call drawSegment (auxiliary function) for each set of 4 points.
void Bspline::draw(int levelOfDetail) {
	
	connectTheDots();
	/* YOUR CODE HERE */
	Pvector::iterator p;

	p = points.begin();

	if (points.size() > 3){
		for (int i = 0; i < points.size() - 3; i++){
			drawSegment(p + i, p + i + 1, p + i + 2, p + i + 3, levelOfDetail);
		}
	}
}

void Bspline::drawSegment(Pvector::iterator p1, Pvector::iterator p2, Pvector::iterator p3, Pvector::iterator p4, int levelOfDetail) {

	float x, y;
	/* YOUR CODE HERE */
	int degree = 3;
	int k;
	vector<Point> c;

	for (int j = 0; j <= levelOfDetail; j++){
		//vector<vector<Point>> tempP(degree + 1);

		float u = (float)j / levelOfDetail;
		float uC = u*u*u;
		float uS = u*u;
		float tempA = (-uC + 3*uS - 3*u + 1) / 6;
		float tempB = (3*uC - 6*uS + 4) / 6;
		float tempC = (-3*uC + 3*uS + 3*u + 1) / 6;
		float tempD = uC/6;

		float newX = tempA * p1->x + tempB * p2->x + tempC * p3->x + tempD * p4->x;
		float newY = tempA * p1->y + tempB * p2->y + tempC * p3->y + tempD * p4->y;

		c.push_back(Point(newX, newY));
	}

	//draw segment
	for (int i = 0; i < c.size() - 1; i++){
		drawLine(c[i].x, c[i].y, c[i + 1].x, c[i + 1].y);
	}
	
	x = c.back().x;
	y = c.back().y;
	//then create a Point to be drawn where the knot should be

	Point p(x, y);
	p.draw();
}

#include "Bezier2.h"

//This function is provided to aid you.
//It should be used in the spirit of recursion, though you may choose not to.
//This function takes an empty vector of points, accum
//It also takes a set of control points, pts, and fills accum with
//the control points that correspond to the next level of detail.
void accumulateNextLevel(Pvector* accum, Pvector pts) {
	if (pts.empty()) 
		return; 
	accum->push_back(*(pts.begin()));
	if (pts.size() == 1) 
		return;
	for (Pvector::iterator it = pts.begin(); it != pts.end() - 1; it++) {
		/* YOUR CODE HERE  (only one to three lines)*/
		it->x = 0.5*(it->x + (it+1)->x);
		it->y = 0.5*(it->y + (it+1)->y);
	}
	//save the last point
	Point last = *(pts.end()-1);
	pts.pop_back();
	//recursive call
	accumulateNextLevel(accum, pts);
	accum->push_back(last);
}


// The basic draw function for Bezier2.  Note that as opposed to Bezier, 
// this draws the curve by recursive subdivision.  So, levelofdetail 
// corresponds to how many times to recurse.  
void Bezier2::draw(int levelOfDetail) {
	//This is just a trick to find out if this is the top level call
	//All recursive calls will be given a negative integer, to be flipped here
	if (points.size() > 1){
		if (levelOfDetail > 0) {
			connectTheDots();
		} else {
			levelOfDetail = -levelOfDetail;
		}

		//Base case.  No more recursive calls.
		if (levelOfDetail <= 1) {
			if (points.size() >= 2) {
				for (Pvector::iterator it = points.begin(); it != points.end() - 1; it++) {
					/* YOUR CODE HERE */
					drawLine(it->x, it->y, (it+1)->x, (it+1)->y);
				}
			}
		} else {
			Pvector* accum = new Pvector();
			Bezier2 left, right;

			//add the correct points to 'left' and 'right'.
			//You may or may not use accum as you see fit.
			/* YOUR CODE HERE */
			accumulateNextLevel(accum, points);
			Pvector::iterator p = accum->begin();

			for (int i = 0; i <= accum->size() / 2; i++){
				left.points.push_back(accum->at(i));
			}

			for (int i = accum->size() / 2; i < accum->size(); i++){
				right.points.push_back(accum->at(i));
			}

			left.draw(1-levelOfDetail);
			right.draw(1-levelOfDetail);
			delete accum;
		}
	}
}
