#pragma once

#include "Curve.h"
#include "Model.h"

class ModelScene
{
public:
	ModelScene(void);
	~ModelScene(void);
private:
	int mWidth;
	int mHeight;
	Model* mModel;
	Curve* mCurve;
};

